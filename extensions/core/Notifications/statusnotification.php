<?php
/**
 * @brief		Notification Options
 * @author		<a href='http://www.invisionpower.com'>Invision Power Services, Inc.</a>
 * @copyright	(c) 2001 - 2016 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/legal/standards/
 * @package		IPS Community Suite
 * @subpackage	Server Console
 * @since		25 Feb 2017
 * @version		SVN_VERSION_NUMBER
 */

namespace IPS\serverconsole\extensions\core\Notifications;

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	header( ( isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0' ) . ' 403 Forbidden' );
	exit;
}

/**
 * Notification Options
 */
class _statusnotification
{


    /**
     * Get configuration
     *
     * @param	\IPS\Member	$member	The member
     * @return	array
     */
    public function getConfiguration( $member )
    {
        $return = array();
        $groups = array();

        $allowedGroups = explode(',', \IPS\Settings::i()->serverconsole_notification_groups);
        foreach(\IPS\Member\Group::groups(TRUE, FALSE) as $k => $g) {
            if(in_array($k, $allowedGroups) || \IPS\Settings::i()->serverconsole_notification_groups === 'all') {
                $groups[] = $k;
            }
        }

        if ($member === null || ($member && in_array($member->group['g_id'], $groups)))
        {
            $return['statuschange_nodedown'] = array( 'default' => array( 'inline' ), 'disabled' => array() );
            $return['statuschange_nodeup'] = array( 'default' => array( 'inline' ), 'disabled' => array() );

            if ( \IPS\Settings::i()->serverconsole_notification_on_maintenance ) {
                $return['statuschange_mnt_start'] = array( 'default' => array( 'inline' ), 'disabled' => array() );
                $return['statuschange_mnt_finished'] = array( 'default' => array( 'inline' ), 'disabled' => array() );
            }
        }

        return $return;
    }

    // For each type of notification you need a method like this which controls what will be displayed when the user clicks on the notification icon in the header:
    // Note that for each type of notification you must *also* create email templates. See documentation for details: https://remoteservices.invisionpower.com/docs/devdocs-notifications

    /**
     * Parse notification: statuschange_nodedown
     *
     * @param	\IPS\Notification\Inline	$notification	The notification
     * @return	array
     * @endcode
     */
    public function parse_statuschange_nodedown( \IPS\Notification\Inline $notification )
    {
        $item = $notification->item;
        return array(
            'title'		    => $item->name . ' is no longer reachable. It appears to be offline.',
            'url'			=> \IPS\Http\Url::internal( 'app=serverconsole&module=general&controller=statuspage' )
        );
    }
    /**
     * Parse notification: statuschange_nodedown
     *
     * @param	\IPS\Notification\Inline	$notification	The notification
     * @return	array
     * @endcode
     */
    public function parse_statuschange_nodeup( \IPS\Notification\Inline $notification )
    {
        $item = $notification->item;
        return array(
            'title'		    => $item->name . ' is back up!',
            'url'			=> \IPS\Http\Url::internal( 'app=serverconsole&module=general&controller=statuspage' )
        );
    }
    /**
     * Parse notification: statuschange_nodedown
     *
     * @param	\IPS\Notification\Inline	$notification	The notification
     * @return	array
     * @endcode
     */
    public function parse_statuschange_mnt_start( \IPS\Notification\Inline $notification )
    {
        $item = $notification->item;
        return array(
            'title'		    => $item->name . ' is now in maintenance mode',
            'url'			=> $item->url(),
            'content'       => $item->maintenanceInfo,
            'author'		=> \IPS\Member::loggedIn()
        );
    }
    /**
     * Parse notification: statuschange_nodedown
     *
     * @param	\IPS\Notification\Inline	$notification	The notification
     * @return	array
     * @endcode
     */
    public function parse_statuschange_mnt_finished( \IPS\Notification\Inline $notification )
    {
        $item = $notification->item;
        return array(
            'title'		    => $item->name . ' is no longer under maintenance',
            'url'			=> $item->url(),
            'author'		=> \IPS\Member::loggedIn()
        );
    }
}