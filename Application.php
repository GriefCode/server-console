<?php
/**
 * @brief		Server Console Application Class
 * @author		<a href='https://dota.vision/'>GriefCode</a>
 * @copyright	(c) 2017 GriefCode
 * @package		IPS Community Suite
 * @subpackage	Server Console
 * @since		22 Feb 2017
 * @version		
 */
 
namespace IPS\serverconsole;

/**
 * Server Console Application Class
 */
class _Application extends \IPS\Application
{
	
}