<?php
/**
 * @brief		pingservers Task
 * @author		<a href='http://www.invisionpower.com'>Invision Power Services, Inc.</a>
 * @copyright	(c) 2001 - 2016 Invision Power Services, Inc.
 * @license		http://www.invisionpower.com/legal/standards/
 * @package		IPS Community Suite
 * @subpackage	serverconsole
 * @since		22 Feb 2017
 * @version		SVN_VERSION_NUMBER
 */

namespace IPS\serverconsole\tasks;

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	header( ( isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0' ) . ' 403 Forbidden' );
	exit;
}

/**
 * pingservers Task
 */
class _pingservers extends \IPS\Task
{
	/**
	 * Execute
	 *
	 * If ran successfully, should return anything worth logging. Only log something
	 * worth mentioning (don't log "task ran successfully"). Return NULL (actual NULL, not '' or 0) to not log (which will be most cases).
	 * If an error occurs which means the task could not finish running, throw an \IPS\Task\Exception - do not log an error as a normal log.
	 * Tasks should execute within the time of a normal HTTP request.
	 *
	 * @return	mixed	Message to log or NULL
	 * @throws	\IPS\Task\Exception
	 */
	public function execute()
	{
	    if(\IPS\Settings::i()->serverconsole_pingservers) {
	        return NULL;
        }

	    foreach(\IPS\Db::i()->select('id, ip, ping_port', 'serverconsole_servers', ['ping_server=?', 1]) as $server) {
            $errno = $errstr = '';
            $fp = fSockOpen($server['ip'], $server['ping_port'], $errno, $errstr, 1);
            if($fp) {
                \IPS\Db::i()->update('serverconsole_servers', ['status' => 'online', 'last_ping' => date('Y-m-d H:i:s')], ['id=?', $server['id']]);
                fclose($fp);
            } else {
                \IPS\Db::i()->update('serverconsole_servers', ['status' => 'offline'], ['id=?', $server['id']]);
            }
        }

		return NULL;
	}
	
	/**
	 * Cleanup
	 *
	 * If your task takes longer than 15 minutes to run, this method
	 * will be called before execute(). Use it to clean up anything which
	 * may not have been done
	 *
	 * @return	void
	 */
	public function cleanup()
	{
		
	}
}