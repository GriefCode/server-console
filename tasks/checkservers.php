<?php
/**
 * @brief    checkservers Task
 * @author    <a href='http://www.invisionpower.com'>Invision Power Services, Inc.</a>
 * @copyright  (c) 2001 - 2016 Invision Power Services, Inc.
 * @license    http://www.invisionpower.com/legal/standards/
 * @package    IPS Community Suite
 * @subpackage  serverconsole
 * @since    23 Feb 2017
 * @version    SVN_VERSION_NUMBER
 */

namespace IPS\serverconsole\tasks;

/* To prevent PHP errors (extending class does not exist) revealing path */
use TrueBV\Exception\OutOfBoundsException;

if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * checkservers Task
 */
class _checkservers extends \IPS\Task
{
    const STATUS_ONLINE = 'online';
    const STATUS_OFFLINE = 'offline';

    /**
     * Execute
     *
     * If ran successfully, should return anything worth logging. Only log something
     * worth mentioning (don't log "task ran successfully"). Return NULL (actual NULL, not '' or 0) to not log (which will be most cases).
     * If an error occurs which means the task could not finish running, throw an \IPS\Task\Exception - do not log an error as a normal log.
     * Tasks should execute within the time of a normal HTTP request.
     *
     * @return  mixed  Message to log or NULL
     * @throws  \IPS\Task\Exception
     */
    public function execute()
    {

        if (!\IPS\Settings::i()->serverconsole_checkservers) {
            return NULL;
        }

        $conditionString = 'check_server=?';
        if (\IPS\Settings::i()->serverconsole_check_during_maintenance) {
            $conditionString .= ' AND in_maintenance=0';
        }

        foreach (\IPS\Db::i()->select('id, last_status_history', 'serverconsole_servers', [$conditionString, 1]) as $dbServer) {
            $server = \IPS\serverconsole\Items\Server::load($dbServer['id']);
            try {
                $previousStatus = \IPS\serverconsole\Records\ServerHistory::load($dbServer['last_status_history']);
            } catch (\OutOfRangeException $e) {
                $previousStatus = false;
            }

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $server->status_script_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HEADER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT_MS, \IPS\Settings::i()->serverconsole_servercheck_timeout);
            $result = curl_exec($curl);
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $body = mb_substr($result, $header_size);

            if (FALSE === $result || $httpcode !== 200) {
                if ($previousStatus && $previousStatus->status !== self::STATUS_OFFLINE) {
                    if ($server->tolerance_counter >= \IPS\Settings::i()->serverconsole_servercheck_tolerance) {
                        $history = new \IPS\serverconsole\Records\ServerHistory;
                        $history->serverconsole_servers_id = $server->id;
                        $history->status = self::STATUS_OFFLINE;

                        $history->save();
                        $server->tolerance_counter = 0;
                        $server->last_status_history = $history->id;
                        $server->save();

                        $server->notifyNodeDown();
                        if ($httpcode !== 200) {
                            throw new \RuntimeException("The script could not be reached, please check your script path. \n Response Code: ${httpcode}\nResponse: ${body}");
                        }
                    } else {
                        $server->tolerance_counter++;
                        $server->save();
                    }
                }
            } else {
                if ($previousStatus && $previousStatus->status !== self::STATUS_ONLINE) {
                    if ($server->tolerance_counter >= \IPS\Settings::i()->serverconsole_servercheck_tolerance) {
                        $content = json_decode($body, true);

                        $history = new \IPS\serverconsole\Records\ServerHistory;
                        $history->serverconsole_servers_id = $server->id;

                        $history->cpu_load_now = $content['cpu']['load'][0];
                        $history->cpu_load_5min = $content['cpu']['load'][1];
                        $history->cpu_load_15min = $content['cpu']['load'][2];
                        $history->cpu_usage = $content['cpu']['usage'];
                        $history->ram_used = $content['ram']['used'];
                        $history->ram_total = $content['ram']['total'];
                        $history->ram_percentage = $content['ram']['percentage'];
                        $history->diskspace_free = $content['diskspace']['free'];
                        $history->diskspace_total = $content['diskspace']['total'];
                        $history->diskspace_percentage = $content['diskspace']['percentage'];
                        $history->uptime = $content['uptime'];
                        $history->status = self::STATUS_ONLINE;

                        $history->save();
                        $server->tolerance_counter = 0;
                        $server->last_status_history = $history->id;
                        $server->save();

                        if ($previousStatus && $previousStatus->status !== self::STATUS_ONLINE) {
                            $server->notifyNodeUp();
                        }
                    } else {
                        $server->tolerance_counter++;
                        $server->save();
                    }
                } else {
                    $content = json_decode($body, true);

                    $history = new \IPS\serverconsole\Records\ServerHistory;
                    $history->serverconsole_servers_id = $server->id;

                    $history->cpu_load_now = $content['cpu']['load'][0];
                    $history->cpu_load_5min = $content['cpu']['load'][1];
                    $history->cpu_load_15min = $content['cpu']['load'][2];
                    $history->cpu_usage = $content['cpu']['usage'];
                    $history->ram_used = $content['ram']['used'];
                    $history->ram_total = $content['ram']['total'];
                    $history->ram_percentage = $content['ram']['percentage'];
                    $history->diskspace_free = $content['diskspace']['free'];
                    $history->diskspace_total = $content['diskspace']['total'];
                    $history->diskspace_percentage = $content['diskspace']['percentage'];
                    $history->uptime = $content['uptime'];
                    $history->status = self::STATUS_ONLINE;

                    $history->save();
                    $server->tolerance_counter = 0;
                    $server->last_status_history = $history->id;
                    $server->save();
                }
            }
            curl_close($curl);

        }
        return NULL;
    }

    /**
     * Cleanup
     *
     * If your task takes longer than 15 minutes to run, this method
     * will be called before execute(). Use it to clean up anything which
     * may not have been done
     *
     * @return  void
     */
    public function cleanup()
    {

    }
}
