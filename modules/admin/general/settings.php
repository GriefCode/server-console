<?php

namespace IPS\serverconsole\modules\admin\general;

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * settings
 */
class _settings extends \IPS\Dispatcher\Controller
{

    const NONE = 'none';
    const ALL = 'all';

    /**
     * Execute
     *
     * @return  void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission('settings_manage');
        parent::execute();
    }

    /**
     * @return  void
     */
    protected function manage()
    {
        $settings = new \IPS\Helpers\Form('serverconsole_settings');
        $settings->addTab('serverconsole_settings_general');
        $settings->add(new \IPS\Helpers\Form\YesNo('serverconsole_checkservers',
            \IPS\Settings::i()->serverconsole_checkservers ?: false, true));
        $settings->add(new \IPS\Helpers\Form\YesNo('serverconsole_check_during_maintenance',
            \IPS\Settings::i()->serverconsole_check_during_maintenance ?: false, true));
        $settings->add(new \IPS\Helpers\Form\Number('serverconsole_servercheck_timeout',
            \IPS\Settings::i()->serverconsole_servercheck_timeout ?: 5000, true));
        $settings->add(new \IPS\Helpers\Form\Number('serverconsole_servercheck_tolerance',
            \IPS\Settings::i()->serverconsole_servercheck_tolerance ?: 1, true));


        $options = ['CPU Load', 'CPU', 'RAM', 'DISK'];
        $settings->addTab('serverconsole_settings_statuspage');
        $settings->add(new \IPS\Helpers\Form\Text('serverconsole_statuspage_name', \IPS\Settings::i()->serverconsole_statuspage_name ?: 'Network Status', true));
        $settings->add(new \IPS\Helpers\Form\YesNo('serverconsole_statuspage_show_in_footer', \IPS\Settings::i()->serverconsole_statuspage_show_in_footer ?: false, true));
        $settings->add(new \IPS\Helpers\Form\TextArea('serverconsole_statuspage_desc', \IPS\Settings::i()->serverconsole_statuspage_desc ?: 'Check our current server status here', true));
        $settings->add(new \IPS\Helpers\Form\Select('serverconsole_statuspage_first_info', \IPS\Settings::i()->serverconsole_statuspage_first_info ?: self::NONE, true, array(
            'options' => $options,
            'parse' => 'normal',
            'unlimited' => self::NONE,
            'unlimitedLang' => 'serverconsole_statuspage_info_none'
        )));
        $settings->add(new \IPS\Helpers\Form\Select('serverconsole_statuspage_second_info', \IPS\Settings::i()->serverconsole_statuspage_second_info ?: self::NONE, true, array(
            'options' => $options,
            'parse' => 'normal',
            'unlimited' => self::NONE,
            'unlimitedLang' => 'serverconsole_statuspage_info_none'
        )));
        $settings->addTab('serverconsole_settings_notifications');
        $groups = [];
        foreach ( \IPS\Member\Group::groups(TRUE, FALSE) as $k => $v )
        {
            $groups[ $k ] = $v->name;
        }
        $settings->add(new \IPS\Helpers\Form\Select('serverconsole_notification_groups', \IPS\Settings::i()->serverconsole_notification_groups ?: self::ALL, true, array(
            'options' => $groups,
            'parse' => 'normal',
            'multiple' => true,
            'unlimited' => self::ALL,
            'unlimitedLang' => 'serverconsole_notification_all'
        )));
        $settings->add(new \IPS\Helpers\Form\YesNo('serverconsole_notification_on_maintenance', \IPS\Settings::i()->serverconsole_notification_on_maintenance ?: false, true));

        if ($values = $settings->values()) {
            $settings->saveAsSettings();
        }
        /* Output */
        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->addToStack('serverconsole_settings');
        \IPS\Output::i()->output = $settings;
    }
}
