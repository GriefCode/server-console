<?php

namespace IPS\serverconsole\modules\admin\general;

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * servers
 */
class _servers extends \IPS\Dispatcher\Controller
{
    const ALL = 'all';

    /**
     * Execute
     *
     * @return  void
     */
    public function execute()
    {
        \IPS\Dispatcher::i()->checkAcpPermission('servers_manage');
        parent::execute();
    }

    /**
     * Manage
     *
     * @return  void
     */
    protected function manage()
    {
        /* Create the table */
        $table = new \IPS\Helpers\Table\Db('serverconsole_servers',
            \IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers'));

        $table->joins = array(
            array(
                'select' => 'history.cpu_usage, history.ram_percentage, history.diskspace_percentage, history.uptime, history.status, history.cpu_load_now, history.cpu_load_5min, history.cpu_load_15min',
                'from' => array('serverconsole_server_check_history', 'history'),
                'where' => array('history.id=serverconsole_servers.last_status_history')
            )
        );

        $table->langPrefix = 'serverconsole_servers_table_';
        $table->include =
            array('id', 'name', 'uptime', 'cpu_load_now', 'cpu_usage', 'ram_percentage', 'diskspace_percentage');
        $table->mainColumn = 'id';

        /* Root buttons */
        $table->rootButtons = array(
            'add' => array(
                'icon' => 'cross',
                'title' => 'serverconsole_servers_table_add',
                'link' => \IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers&do=perform'),
            )
        );

        /* Formatters */
        $table->parsers = array(
            'uptime' => function ($val) {
                return isset($val) ? $val . ' Days' : 'unknown';
            },
            'cpu_load_now' => function ($val, $row) {
                return isset($val) ? implode(', ', [$val, $row['cpu_load_5min'], $row['cpu_load_15min']]) : 'unknown';
            },
            'cpu_usage' => function ($val) {
                return isset($val) ? $val . '%' : 'unknown';
            },
            'ram_percentage' => function ($val) {
                return isset($val) ? $val . '%' : 'unknown';
            },
            'diskspace_percentage' => function ($val) {
                return isset($val) ? $val . '%' : 'unknown';
            },
        );

        /* Row buttons */
        $table->rowButtons = function ($row) {
            $return = [];

            if ($row['in_maintenance']) {
                $return['finishMaintenance'] = array(
                    'icon' => 'check',
                    'title' => 'serverconsole_servers_table_finish_maintenance',
                    'link' => \IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers&do=finishMaintenance')
                        ->setQueryString('id', $row['id']),
                    'data' => array('data-confirm')
                );
            } else {
                $return['startMaintenance'] = array(
                    'icon' => 'wrench',
                    'title' => 'serverconsole_servers_table_set_maintenance',
                    'link' => \IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers&do=startMaintenance')
                        ->setQueryString('id', $row['id']),
                    'data' => array('ipsDialog' => '', 'ipsDialog-title' => \IPS\Member::loggedIn()->language()
                        ->addToStack('serverconsole_servers_table_set_maintenance'))
                );
            }

            $return['edit'] = array(
                'icon' => 'pencil',
                'title' => 'serverconsole_servers_table_edit',
                'link' => \IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers&do=perform')
                    ->setQueryString('id', $row['id']),
                'data' => array('ipsDialog' => '', 'ipsDialog-title' => \IPS\Member::loggedIn()->language()
                    ->addToStack('serverconsole_servers_edit'))
            );
            $return['delete'] = array(
                'icon' => 'times-circle',
                'title' => 'serverconsole_servers_table_delete',
                'link' => \IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers&do=delete')
                    ->setQueryString('id', $row['id']),
                'data' => array('delete' => '')
            );
            return $return;
        };

        /* Display */
        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->addToStack('menu__serverconsole_general_manage');
        \IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('global', 'core')->block('title', (string)$table);
    }

    /**
     * Patch/Put Server
     * Creates / Edits a server
     *
     * @return void
     */
    protected function perform()
    {
        try {
            $srv = \IPS\serverconsole\Items\Server::load(\IPS\Request::i()->id);
        } catch (\OutOfRangeException $e) {
            $srv = null;
        }

        $form = new \IPS\Helpers\Form();
        $form->addHeader('serverconsole_servers_details');
        $form->add(new \IPS\Helpers\Form\Text('serverconsole_servers_add_server', $srv ? $srv->name : '', true));
        $form->add(new \IPS\Helpers\Form\Url('serverconsole_servers_add_domain', $srv ? $srv->domain : '', true));
        $form->add(new \IPS\Helpers\Form\Text('serverconsole_servers_add_ip', $srv ? $srv->ip : '', false, array(),
            function ($val) {
                $valid = filter_var($val, FILTER_VALIDATE_IP);
                if (!$valid && !empty($val)) {
                    throw new \InvalidArgumentException('serverconsole_servers_add_ip_invalid');
                }
            }));
        if (\IPS\Settings::i()->serverconsole_checkservers) {
            $form->add(new \IPS\Helpers\Form\YesNo('serverconsole_servers_add_check_server', $srv ? $srv->check_server : ''));
            $form->add(new \IPS\Helpers\Form\Url('serverconsole_servers_add_status_script',
                $srv ? $srv->status_script_url : '', true));
        }
        $form->add(new \IPS\Helpers\Form\TextArea('serverconsole_servers_add_comment', $srv ? $srv->comment : '', false));


        $selectionGroups = [];
        foreach(\IPS\Member\Group::groups(TRUE, FALSE) as $k => $g) {
            $selectionGroups[$k] = $g->name;
        }

        $form->add(new \IPS\Helpers\Form\Select('serverconsole_servers_notification_groups', $srv && $srv->notification_groups !== self::ALL ? explode(',', $srv->notification_groups) : self::ALL, true, array(
            'options' => $selectionGroups,
            'parse' => 'normal',
            'multiple' => true,
            'unlimited' => self::ALL,
            'unlimitedLang' => 'serverconsole_notification_all'
        )));

        if ($values = $form->values()) {
            if (!$srv) {
                $srv = new \IPS\serverconsole\Items\Server;
            }

            $srv->name = $values['serverconsole_servers_add_server'];
            $srv->domain = $values['serverconsole_servers_add_domain'];
            $srv->ip = $values['serverconsole_servers_add_ip'];
            $srv->comment = $values['serverconsole_servers_add_comment'];
            $srv->notification_groups = $values['serverconsole_servers_notification_groups'] === 'all' ? self::ALL : implode(',', $values['serverconsole_servers_notification_groups']);
            if (\IPS\Settings::i()->serverconsole_checkservers) {
                $srv->check_server = $values['serverconsole_servers_add_check_server'];
                $srv->status_script_url = $values['serverconsole_servers_add_status_script'];
            }

            $srv->save();
            \IPS\Output::i()->redirect(\IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers'),
                \IPS\Request::i()->id ? 'serverconsole_servers_add_edited' : 'serverconsole_servers_add_added');
        }

        $title = $srv ? 'serverconsole_servers_edit' : 'serverconsole_servers_add';
        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->addToStack($title);
        \IPS\Output::i()->output = (string)$form;
    }

    /**
     * Delets a server
     */
    protected function delete()
    {
        try {
            $srv = \IPS\serverconsole\Items\Server::load(\IPS\Request::i()->id);
        } catch (\OutOfRangeException $e) {
            throw new \InvalidArgumentException('Could not locate given server.');
        }

        $srv->delete();
        \IPS\Output::i()->redirect(\IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers'),
            'serverconsole_servers_table_delete_deleted');
    }

    /**
     * Puts a server into maintenance mode
     */
    protected function startMaintenance()
    {
        try {
            $srv = \IPS\serverconsole\Items\Server::load(\IPS\Request::i()->id);
        } catch (\OutOfRangeException $e) {
            $srv = null;
        }

        $form = new \IPS\Helpers\Form();
        $form->add(new \IPS\Helpers\Form\TextArea('serverconsole_maintenance_info', '', false));
        $form->add(new \IPS\Helpers\Form\Date('serverconsole_maintenance_estimate_finish', '', false, array('time' => true)));

        if ($values = $form->values()) {
            $srv->in_maintenance = true;
            $srv->maintenance_info = $values['serverconsole_maintenance_info'];
            $srv->maintenance_estimated_finished_time = $values['serverconsole_maintenance_estimate_finish'] ? date("Y-m-d H:i:s", strtotime($values['serverconsole_maintenance_estimate_finish'])) : null;

            $srv->save();

            if (\IPS\Settings::i()->serverconsole_notification_on_maintenance) {
                $srv->notifyMaintenanceStarted();
            }

            \IPS\Output::i()->redirect(\IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers'),
                'serverconsole_maintenance_active');
        }

        \IPS\Output::i()->output = (string)$form;
    }

    /**
     * Finishes a maintenance
     */
    protected function finishMaintenance()
    {
        try {
            $srv = \IPS\serverconsole\Items\Server::load(\IPS\Request::i()->id);
        } catch (\OutOfRangeException $e) {
            $srv = null;
        }
        $srv->in_maintenance = false;
        $srv->maintenance_info = null;
        $srv->maintenance_estimated_finished_time = null;

        $srv->save();

        if (\IPS\Settings::i()->serverconsole_notification_on_maintenance) {
            $srv->notifyMaintenanceFinished();
        }

        \IPS\Output::i()->redirect(\IPS\Http\Url::internal('app=serverconsole&module=general&controller=servers'),
            'serverconsole_maintenance_finished');

    }
}
