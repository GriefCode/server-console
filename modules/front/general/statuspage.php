<?php


namespace IPS\serverconsole\modules\front\general;

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * statuspage
 */
class _statuspage extends \IPS\Dispatcher\Controller
{
    /**
     * Execute
     *
     * @return    void
     */
    public function execute()
    {

        parent::execute();
    }

    /**
     * @return    void
     */
    protected function manage()
    {
        $pageName = \IPS\Settings::i()->serverconsole_statuspage_name;
        $pageDesc = \IPS\Settings::i()->serverconsole_statuspage_desc;
        $servers = [];
        foreach (\IPS\Db::i()->select('*', 'serverconsole_servers', ['check_server=?', 1]) as $server) {
            try {
                $history = \IPS\serverconsole\Records\ServerHistory::load($server['last_status_history']);
                $history->checked_at = strtotime($history->checked_at);
            } catch (\OutOfRangeException $e) {
                $history = null;
            }
            $server['history'] = $history;
            $servers[] = $server;
        }
        /* Display */
        \IPS\Output::i()->cssFiles = array_merge(\IPS\Output::i()->cssFiles, \IPS\Theme::i()->css('statuspage.css', 'serverconsole', 'front'));
        \IPS\Output::i()->title = \IPS\Member::loggedIn()->language()->addToStack(\IPS\Settings::i()->serverconsole_statuspage_name);
        \IPS\Output::i()->breadcrumb[] = array(NULL, \IPS\Output::i()->title);
        \IPS\Output::i()->output = \IPS\Theme::i()->getTemplate('serverstatus', 'serverconsole', 'front')->list($pageName, $pageDesc, $servers);
    }
}