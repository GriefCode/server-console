<?php

namespace IPS\serverconsole\Records;

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
    header( ( isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0' ) . ' 403 Forbidden' );
    exit;
}

class _ServerHistory extends \IPS\Patterns\ActiveRecord {

    /**
     * @brief    [ActiveRecord] ID Database Column
     */
    public static $databaseColumnId = 'id';

    /**
     * @brief	[ActiveRecord] Database ID Fields
     */
    protected static $databaseIdFields = array( 'id' );

    /**
     * @brief    [ActiveRecord] Database table
     * @note    This MUST be over-ridden
     */
    public static $databaseTable = 'serverconsole_server_check_history';

    /**
     * Set Default Values (overriding $defaultValues)
     *
     * @return    void
     */
    protected function setDefaultValues() {}
}
