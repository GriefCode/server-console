<?php
namespace IPS\serverconsole\Items;

class _Server extends \IPS\Content\Item {

    /**
     * @brief Multiton Store
     */
    protected static $multitons;

    /**
     * @brief Default Values
     */
    protected static $defaultValues = NULL;

    /**
     * @brief Application
     */
    public static $application = 'serverconsole';

    /**
     * @brief Module
     */
    public static $module = 'general';

    /**
     * @brief Database Table
     */
    public static $databaseTable = 'serverconsole_servers';

    /**
     * @brief Database Prefix
     */
    public static $databasePrefix = '';

    /**
     * @brief Database Column Map
     */
    public static $databaseColumnMap = array(
        'id' => 'id',
        'name' => 'name',
        'domain' => 'domain',
        'ip' => 'ip',
        'comment' => 'comment',
        'checkServer' => 'check_server',
        'lastStatus' => 'last_status_history',
        'statusScript' => 'status_script_url',
        'inMaintenance' => 'in_maintenance',
        'maintenanceInfo' => 'maintenance_info',
        'maintenanceFinish' => 'maintenance_estimated_finished_time',
        'notificationGroup' => 'notification_groups',
    );

    /**
     * @brief Title
     */
    public static $title = 'server';

	/**
     * Get URL
     *
     * @param string|NULL
     * @return \IPS\Http\Url
     */
	public function url( $action=NULL )
    {
        $url = \IPS\Http\Url::internal("app=serverconsole&module=general&controller=statuspage");
        return $url;
    }

    /**
     * Notification that the maintenance has started
     */
    public function notifyMaintenanceStarted()
    {
        $notification = new \IPS\Notification(\IPS\Application::load('serverconsole'), 'statuschange_mnt_start', $this, array($this));
        $this->_prepateNotificationAndSend($notification);
    }

    /**
     * Notification that a maintenance has been finished
     */
    public function notifyMaintenanceFinished()
    {
        $notification = new \IPS\Notification(\IPS\Application::load('serverconsole'), 'statuschange_mnt_finished', $this, array($this));
        $this->_prepateNotificationAndSend($notification);
    }

    /**
     * Notification when a server is no longer reachable
     */
    public function notifyNodeDown()
    {
        $notification = new \IPS\Notification(\IPS\Application::load('serverconsole'), 'statuschange_nodedown', $this, array($this));
        $this->_prepateNotificationAndSend($notification);
    }

    /**
     * Notification when a server is no longer reachable
     */
    public function notifyNodeUp()
    {
        $notification = new \IPS\Notification(\IPS\Application::load('serverconsole'), 'statuschange_nodeup', $this, array($this));
        $this->_prepateNotificationAndSend($notification);
    }

    /**
     * Helper method to collect all member which should recieve notifications and send it
     * @param \IPS\Notification $notification
     */
    protected function _prepateNotificationAndSend(\IPS\Notification $notification)
    {
        $set = $groups = $where = [];
        $allowedGroups = explode(',', \IPS\Settings::i()->serverconsole_notification_groups);
        $allowedGroups = array_merge($allowedGroups, explode(',', $this->notification_groups));
        foreach (\IPS\Member\Group::groups(TRUE, FALSE) as $k => $g) {
            if (in_array($k, $allowedGroups) || \IPS\Settings::i()->serverconsole_notification_groups === 'all' || $this->notification_groups === 'all' ) {
                $set[] = "FIND_IN_SET(" . $g->g_id . ",mgroup_others)";
                $groups[] = $g->g_id;
            }
        }
        if (count($set)) {
            $where[] = array("( member_group_id IN(" . implode(',', $groups) . ") OR " . implode(' OR ', $set) . ' )');
        }
        foreach (\IPS\Db::i()->select('*', 'core_members', $where) as $member) {
            $notification->recipients->attach(\IPS\Member::constructFromData($member));
        }
        $notification->send();
    }
}