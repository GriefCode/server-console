//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class serverconsole_hook_footer_hook extends _HOOK_CLASS_
{

/* !Hook Data - DO NOT REMOVE */
public static function hookData() {
 return array_merge_recursive( array (
  'footer' => 
  array (
    0 => 
    array (
      'selector' => '#elFooterLinks',
      'type' => 'add_inside_start',
      'content' => '{{if \IPS\Settings::i()->serverconsole_statuspage_show_in_footer}}
  <li>
    <a href="{url="app=serverconsole&module=general&controller=statuspage" seoTemplate="serverconsole_statuspage"}">
    	{{$pageName = \IPS\Settings::i()->serverconsole_statuspage_name;}}
    	{$pageName}
  	</a>
  </li>
{{endif}}',
    ),
  ),
), parent::hookData() );
}
/* End Hook Data */


}
