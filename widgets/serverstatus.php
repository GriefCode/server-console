<?php
/**
 * @brief        serverstatus Widget
 * @author        <a href='http://www.invisionpower.com'>Invision Power Services, Inc.</a>
 * @copyright    (c) 2001 - 2016 Invision Power Services, Inc.
 * @license        http://www.invisionpower.com/legal/standards/
 * @package        IPS Community Suite
 * @subpackage    serverconsole
 * @since        23 Feb 2017
 * @version        SVN_VERSION_NUMBER
 */

namespace IPS\serverconsole\widgets;

/* To prevent PHP errors (extending class does not exist) revealing path */
if (!defined('\IPS\SUITE_UNIQUE_KEY')) {
    header((isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0') . ' 403 Forbidden');
    exit;
}

/**
 * serverstatus Widget
 */
class _serverstatus extends \IPS\Widget\StaticCache
{
    const ALL_SERVER = 'all';
    const NO_INFO = 'no_info';
    /**
     * @brief    Widget Key
     */
    public $key = 'serverstatus';

    /**
     * @brief    App
     */
    public $app = 'serverconsole';

    /**
     * @brief    Plugin
     */
    public $plugin = '';

    /**
     * Initialise this widget
     *
     * @return void
     */
    public function init()
    {
        if (isset($this->configuration['cacheTime'])) {
            $caches = static::getCaches($this->key, $this->app, $this->plugin);
            foreach ($caches as $time) {
                if ($time + $this->configuration['cacheTime'] <= time()) {
                    static::deleteCaches($this->key, $this->app, $this->plugin);
                    break;
                }
            }
        }

        parent::init();
    }

    /**
     * Specify widget configuration
     *
     * @param    null|\IPS\Helpers\Form $form Form object
     * @return    null|\IPS\Helpers\Form
     */
    public function configuration(&$form = null)
    {
        if ($form === null) {
            $form = new \IPS\Helpers\Form;
        }

        $servers = [];
        foreach (\IPS\Db::i()->select('*', 'serverconsole_servers', ['check_server=?', 1]) as $server) {
            $servers[$server['id']] = $server['name'];
        }

        $form->add(
            new \IPS\Helpers\Form\Select(
                'serverconsole_statuswidget_servers',
                isset($this->configuration['serverconsole_statuswidget_servers']) ? $this->configuration['serverconsole_statuswidget_servers'] : self::ALL_SERVER,
                true, array(
                    'options' => $servers,
                    'parse' => 'normal',
                    'multiple' => true,
                    'unlimited' => self::ALL_SERVER,
                    'unlimitedLang' => 'serverconsole_statuswidget_all_servers'
                )
            )
        );

        $options = ['CPU Load', 'CPU', 'RAM', 'DISK'];
        $cfg = isset($this->configuration['serverconsole_statuswidget_display']) ? $this->configuration['serverconsole_statuswidget_display'] : self::NO_INFO;
        if ($cfg !== self::NO_INFO) {
            $cfg = (int)$cfg;
        }
        $form->add(
            new \IPS\Helpers\Form\Select(
                'serverconsole_statuswidget_display',
                $cfg,
                true, array(
                    'options' => $options,
                    'parse' => 'normal',
                    'unlimited' => self::NO_INFO,
                    'unlimitedLang' => 'serverconsole_statuswidget_info_nothing'
                )
            )
        );

        $form->add(
            new \IPS\Helpers\Form\Number(
                'serverconsole_statuswidget_cacheTime', isset($this->configuration['serverconsole_statuswidget_cacheTime']) ? $this->configuration['serverconsole_statuswidget_cacheTime'] : 60 * 15,
                true, array('min' => 30)
            )
        );

        return $form;
    }

    /**
     * Ran before saving widget configuration
     *
     * @param    array $values Values from form
     * @return    array
     */
    public function preConfig($values)
    {
        return $values;
    }

    /**
     * Render a widget
     *
     * @return    string
     */
    public function render()
    {
        $servers = $this->getServersToBeShown();
        $config = $this->getConfig();

        return $this->output($servers, $config);
    }

    /**
     * Collects all servers that should be shown based on given settings
     *
     * @return array
     */
    public function getServersToBeShown()
    {
        $config = isset($this->configuration['serverconsole_statuswidget_servers']) ? $this->configuration['serverconsole_statuswidget_servers'] : [];

        $servers = [];
        $serverIds = array_values(is_array($config) ? $config : []);
        foreach (\IPS\Db::i()->select('*', 'serverconsole_servers', ['check_server=?', 1]) as $server) {
            if ($config === self::ALL_SERVER || in_array($server['id'], $serverIds)) {
                try {
                    $history = \IPS\serverconsole\Records\ServerHistory::load($server['last_status_history']);
                } catch (\OutOfRangeException $e) {
                    $history = null;
                }
                $server['history'] = $history;
                $servers[] = $server;
            }
        }

        return $servers;
    }

    /**
     * Gets the config which parameters are visible on the widget
     *
     * @return null|int
     */
    public function getConfig()
    {
        return isset($this->configuration['serverconsole_statuswidget_display']) ? $this->configuration['serverconsole_statuswidget_display'] : null;
    }
}
